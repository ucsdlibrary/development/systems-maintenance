# Systems Maintenance

This repo is for issues and discussions related to systems maintenance, including things like:

- OS upgrades or migrations
- Security updates
- Application updates like:
    * Rancher
    * Aspace
    * Etc
- Certificate rotation

Code snippets associated with these changes should NOT be attached to this repo and most likely belong on nyx ( which is stored in the Infra project repo ).